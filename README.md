# README #

spicy memes is an MIT-licensed utility library for the Ren'Py visual novel engine.

### What is this repository for? ###

* This library contains lots of utilities which extend the functionality provided by the RenPy engine including
* Audio ducking
* lerping
* tons of animations (transforms and ATL functions)
* a ton of transitions

### How do I get set up? ###

You should install this as a git submodule if you're using git. Make sure to ignore git-related files in your `options.rpy` file.

### Contribution guidelines ###

Please contact me if you'd like to contribute, or just create a pull request.

### Who do I talk to? ###

* Contact neko.py on [Discord](https://discord.gg/a2XhkpN)
* See Pantsu Soft's games on [Gamejolt](https://gamejolt.com/@pantsusoft)
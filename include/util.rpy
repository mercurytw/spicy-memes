# Utility functions


init -2 python:

    def create_keymap_hook(fn, keybinding):
        """
        Simple function to add a function that will trigger when the user presses
        something. The keybinding is from (PyGame)[pygame.org] 

        usage example, to attach auto-advance to the "a" key

        ```
        init python:
            create_keymap_hook(Preference("auto-forward", "toggle"), 'a')
        ```
        :param fn: The callable to execute when the key is hit
        :param keybinding: The name of the key to bind the function to
                           These are from PyGame 
        """
        config.overlay_functions.append(lambda: ui.keymap(**{keybinding : fn}))

    def lerp(start, stop, duration, now):
        """
        Does standard linear interpolation (lerp) based on time
        :param start: The initial value
        :param end: The ending value
        :param duration: The amount of time it takes to do the lerp
        :param now: The current time of the lerp
        """
        scaler = now / duration
        scaler = scaler if scaler < 1.0 else 1.0
        return start + ((stop - start) * (scaler))

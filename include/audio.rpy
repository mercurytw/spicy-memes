# Audio stuff

init -1 python:
    renpy.add_layer('meme_timers', above='overlay', menu_clear=False)

    def sound_get_queue_len(channel='sound'):
        l = len(renpy.music.get_channel('sound').queue)
        print l
        return l
        
    ducker_timer = None
    def _ducker_callback(delay, force=False):
        global ducker_timer
        ui.layer('meme_timers')
        ui.remove(ducker_timer)
        ui.close()
        ducker_timer = None
        if not force and renpy.sound.is_playing():
            _lameo_ducker_callback(delay)
            return
        renpy.music.set_volume(1.0, delay=delay)
        renpy.music.set_volume(1.0, delay=delay, channel='amb')
        renpy.restart_interaction()
        
        
    def _lameo_ducker_callback(delay):
        global ducker_timer
        renpy.music.set_queue_empty_callback(None, channel='sound')
        duration = renpy.music.get_duration(channel='sound')
        if 0 == sound_get_queue_len() and 0 >= duration and not renpy.sound.is_playing():
            _ducker_callback(delay, True)
            return
        ui.layer('meme_timers')
        ducker_timer = ui.timer(max(duration, 0.334), partial(_ducker_callback, delay))
        ui.close()
        renpy.restart_interaction()
        
        
    def duck_for_sfx(sound, delay=2.0):
        """Ducks audio channels while a sound effect plays
        :param sound: The name of the sound effect to play
        :param delay: The fade in/out time for the other channels. Default 2.0 seconds
        """
        renpy.music.set_volume(0.0, delay=0.2)
        renpy.music.set_volume(0.0, delay=0.2, channel='amb')
        renpy.music.play(sound, channel='sound')
        renpy.music.set_queue_empty_callback(partial(_lameo_ducker_callback, delay), channel='sound')

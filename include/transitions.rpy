# all manner of spicy transitions

define fastdissolve = Dissolve(0.25)
define slowdissolve = Dissolve(2.0)
define fastdissolvealpha = Dissolve(0.25, alpha=True)
define slowdissolvealpha = Dissolve(2.0, alpha=True)

# see renpy/common/00definitions.rpy in the sdk for deets
define fastmoveoutleft = MoveTransition(0.25, leave=_moveleft, enter=_moveright)
define fastmoveoutright = MoveTransition(0.25, leave=_moveright, enter=_moveleft)
define fastmoveinright = MoveTransition(0.25, enter=_moveright, leave=_moveleft)
define fastmoveinleft = MoveTransition(0.25, enter=_moveleft, leave=_moveright)
define slowmoveinright = MoveTransition(1.5, enter=_moveright, leave=_moveleft)
define slowmoveoutleft = MoveTransition(1.5, leave=_moveleft, enter=_moveright)
define slowmoveoutright = MoveTransition(1.5, leave=_moveright, enter=_moveleft)
define slowmoveinright = MoveTransition(1.5, enter=_moveright, leave=_moveleft)
define slowmoveinleft = MoveTransition(1.5, enter=_moveleft, leave=_moveright)

define slowmove = MoveTransition(1.5)

define pancamright = MoveTransition(0.75, leave=_moveleft, enter=_moveright)
define pancamleft = MoveTransition(0.75, leave=_moveright, enter=_moveleft)


define dollyout = OldMoveTransition(1.5, enter_factory=ZoomInOut(1.5, 1.0))
define dissolvedollyout = ComposeTransition(slowdissolve, before=None, after=dollyout)

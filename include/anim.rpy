# This script contains various types of stock animations




#############################
### ATL Functions
#############################

init -2 python:
    import math
    from functools import partial

    def wiggler(trans, st, at):
        """
        This animation will wiggle the given sprite on the screen, just a little
        This was used in Kokoro 2 for screaming
        
        In ATL, use it like `function wiggler`
        """
        xwiggle = -2 + (renpy.random.random() * 4)
        ywiggle = -2 + (renpy.random.random() * 4)
        trans.xoffset = xwiggle
        trans.yoffset = ywiggle
        return 0.1

    def move_offsets_along_vector(x, y, speed, trans, st, at):
        """
        This animation allows you to specify a velocity, and 
        will move the offsets of the given transform at that
        velocity

        It's used as a binding, so you use it like this:

        ```
        show missile:
            function partial(move_offsets_along_vector(0, 1, 2))
        ```
        that would cause your missle to move vertically (0, 1) 
        at a speed of 2 units per second
        """
        mag = math.sqrt((x*x) + (y*y))
        x = x / mag
        y = y / mag
        trans.xoffset = trans.xoffset + (x * st * speed)
        trans.yoffset = trans.yoffset + (y * st * speed)
        return 0.0334
    
    def run_for(duration, function, trans, st, at):
        """
        This will cause the given animation to only run
        for a certain amount of time. Let's see an example
        with the wiggler function

        ```
        show eileen scared:
            function partial(wiggler, 2)
        ```

        This would cause eileen to wiggle for 2 seconds 
        after being shown
        """
        if st > duration:
            return None
        return function(trans, st, at)
    
    def rocker(period, time_offt_s, max_rotation, trans, st, at):
        """
        Causes the given transform to rock back and forth gently
        period      the period of rotation in seconds
        time_offt_s can be used to adjust the beginning position
                    of the animation, so that if you use this 
                    animation on multiple things on the screen 
                    (such as falling leaves) they arent all syncrhonzied
        max_rotation The (absolute value) maximum rotation of the 
                     animation in degrees

        Also a partial function, this is used like so:

        show boat:
            subpixel True
            function partial(rocker, 5.0, 0.0, 15.0)

        This would cause the boat to rock back and forth by no more
        than 15 degrees with a period of 5 seconds. Since the motion 
        is slow, we use blitting (the subpixel keyword) to prevent
        the motion from appearing choppy.
        Since there's only one boat on screen, I left the offset
        zero.
        """
        st_prime = st + time_offt_s
        t = (math.pi * 2.0) * ((st_prime % period) / period)
        trans.rotate = max_rotation * math.sin(t)
        return 0.0334


    # Camera bob stuff

    def camera_bob_motion(a,b,c,d,e,f,st,freq,big_image, full=True):
        """
        FOR INTERNAL USE

        The idea here is to improve on the magical background
        implementation by being stateless, and storing all
        state in a wrapper function, which we'll use like
        a function binding to set the variables wherever we need it.
        
        Call period is locked at 1/30 of a second
        
        Use declare one-off usages inline like so:
        usage:
        init python:
            jungle_run_fn_image = Image("backgrounds/jungle-run-base.jpg")
            def jungle_run_fn(st, at):
                return (camera_bob_motion(50, 1, 300,
                                          30, 2, 120,
                                          st, 0.75, jungle_run_fn_image))
        scene expression DynamicDisplayable(jungle_run_fn)
        
        @param a coefficient of the result of the sine function (for x)
        @param b coefficient of t before being passed to sine function (for x)
        @param c x-component of translation
        @param d coefficient of the result of the cosine function (for y)
        @param e coefficient of t before being passed to cosine function (for y)
        @param f y-component of translation
        @param st Show-time in seconds of this displayable
        @param freq Frequency at which the t-parameter completes it's cycle, in Hertz
        @param big_image the image we're cropping
        @param full=True is the range of t [0, 2pi] ? otherwise it's half range: [0, pi]
        """
        trange = 2*math.pi
        if not full:
            trange = math.pi
        t = trange * ((freq * st) % 1)
        x = (a*math.sin(b*t)) + c
        y = (d*math.cos(e*t)) + f
        
        return LiveCrop((x, y, 1280, 720), big_image), 0.0334

    camera_bob_running = False
    camera_bob_doubletime = False
    walk_fn_lasttime = -1.0
    walk_fn_timeagg = 0.0
    walk_fn_last_image = None

    def start_camera_bob():
        global camera_bob_running 
        camera_bob_running = True
    
    def stop_camera_bob():
        global camera_bob_running
        camera_bob_running = False

    def bob_doubletime():
        global camera_bob_doubletime 
        camera_bob_doubletime = True

    def bob_normal_speed():
        global camera_bob_doubletime
        camera_bob_doubletime = False
    
    """
    This and the above functions are used for adding a camera-bob background
    to your VN. You set it up with an image that's larger than your 
    window, like this:
    init python:
        sewer_tunnel_walk_fn_image = Image("backgrounds/sewer tunnel.jpg")
        sewer_tunnel_walk_fn = partial(generic_walk_function, sewer_tunnel_walk_fn_image)

    Then, you can use it later like so:
    scene expression DynamicDisplayable(sewer_tunnel_walk_fn)

    When the scene is shown, the camera bob can be turned on with
    $ start_camera_bob()
    or off
    $ stop_camera_bob()
    set the camera bob to make it seem as though the main character is running:
    $ bob_doubletime()
    and set it back to normal:
    $ bob_normal_speed()
    """
    def generic_walk_function(image, st, at):
        global walk_fn_lasttime
        global walk_fn_timeagg
        global walk_fn_last_image
        global camera_bob_running
        camera_bob_base_frequency = 0.5
        camera_bob_doubletime_frequency = 1.0
        if not camera_bob_running:
            walk_fn_lasttime = -1.0
            if walk_fn_last_image is None or image.get_hash() != walk_fn_last_image[1]:
                walk_fn_last_image = (image, 0.0334), image.get_hash()
            return walk_fn_last_image[0]
            
        delta_time = at - walk_fn_lasttime
        if -1 == walk_fn_lasttime:
            delta_time = 0.0
        
        walk_fn_timeagg =  walk_fn_timeagg + delta_time
        walk_fn_lasttime = at
        freq = camera_bob_base_frequency
        if camera_bob_doubletime:
            freq = camera_bob_doubletime_frequency
        walk_fn_last_image = (camera_bob_motion(15, 1, 225,
                                  15, 2, 135,
                                  walk_fn_timeagg, 
                                  freq, image), image.get_hash())
        return walk_fn_last_image[0]



# This spins the given object with the period, in seconds
#
# use like:
# show wheel at spinny(5):
#     align (0.5, 0.5)
#
transform spinny(period):
    subpixel True
    anchor (0.5, 0.5)
    block:
        linear period rotate 360
        rotate 0
        repeat

# This function is used to make things appear at the center
# of the screen. In visual novels, this tends to indicate
# to the player that the object is being inspected by 
# the player character, or is entering or leaving their
# inventory
# note carefully that this should be used with AT, not with
#
# show health_potion at from_inventory

# then later
# hide health_potion
transform from_inventory:
    align (0.5, 0.5)
    on show:
        zoom 0.0
        linear 0.5 zoom 1.0
    on hide:
        linear 0.5 zoom 0.0

# Spins the object in and out, just like the old batman tv show

# This is used the same as `from_inventory` See that for usage notes
transform batman:
    align (0.5, 0.5)
    zoom 0.0
    on show:
        zoom 0.0
        pause 0.4
        linear 0.5 zoom 1.0 rotate -1080
    on hide:
        linear 0.5 zoom 0.0 rotate 0


# This transform causes the sprite to fade in and blink, as though
# they had active camoflauge
transform active_camo:
    alpha 0.0
    ease 1.0 alpha 1.0
    pause 1.75
    alpha 0.0
    pause 1.0
    block:
        alpha 1.0
        pause 0.2
        alpha 0.0
        pause 0.2
        repeat 4
    alpha 0.0



init -2 python:
    def x_scroller_helper(speed, move_left, trans, st, at):
        """
        INTERNAL USE
        """
        lerp_now = st % speed
        if move_left:
            trans.xpos = int(lerp(renpy.config.screen_width, 0, speed, lerp_now))
        else:
            trans.xpos = int(lerp(0, renpy.config.screen_width, speed, lerp_now))
        return 0.0334

# This is for scrolling backgrounds
# To use it, create a horizontally wrappable image that's exactly
# the width and height of your window
# To use it, create a horizontal tile of that image like so.

# You'll notice that we're doubling the width of the image to create the effect

# image bb = im.Tile("backgrounds/sewer tunnel.jpg", (renpy.config.screen_width * 2, renpy.config.screen_height))
# ... later on ...
# scene bb at x_scroller
#
# or to make it scroll to the right, and twice as fast...
# scene bb at x_scroller(5.0, False)

transform x_scroller(speed=10.0, move_left=True):
    anchor (0.5, 0.0)
    subpixel True
    function partial(x_scroller_helper, speed, move_left)


init -2 python:
    def parametric_ellipse(a, b, frequency, rot, at):
        """
        Our ellipse is defined
        x = a cos(t)
        y = b sin(t)
        where t = (0, 2pi)
        """
        cos_rot = math.cos(rot)
        sin_rot = math.sin(rot)
        sqrt_two = math.sqrt(2)
        t = 2*math.pi * ((frequency * at) % 1)
        x = a * math.cos(t)
        y = b * math.sin(t)
        x_prime = (x * cos_rot) - (y * sin_rot)
        y_prime = (x * sin_rot) + (y * cos_rot)
        return x_prime, y_prime

    def elliptic_motion_helper(a, b, frequency, rot, trans, st, at):
        trans.xoffset, trans.yoffset = parametric_ellipse(a, b, frequency, math.radians(rot), at)
        return 0.0334 

# This is used to move a transform in an elliptical motion
# The ellipse is defined using a and b.
# a represents half the width of the ellipse
# b represents half the height of the ellipse
# frequency is how quickly the animation completes a cycle (in hertz)
# rotation is the number of degrees by which we should rotate the ellipse
#
# here's an example:
# 
#    show chen simple at elliptic_motion(300, 100, 1.0, 45.0):
#        align (0.5, 0.5)

# that will have chen orbit the center of the screen quickly in a wide ellipse
# which has been rotated 45 degrees
transform elliptic_motion(a, b, frequency, rotation):
    anchor (0.5, 0.5)
    subpixel True
    function partial(elliptic_motion_helper, a, b, frequency, rotation)
